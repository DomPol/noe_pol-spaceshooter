﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class GOManager : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI score;
	[SerializeField] TextMeshProUGUI record;

    int m_score1;
	int m_score2;

    void Start()
    {
        SetText();
    }

    void SetText()
    {
        m_score1 = PlayerPrefs.GetInt("Score1", 0);
		m_score2 = PlayerPrefs.GetInt("Score2", 0);
		ScoreText(m_score1);
		RecordText(m_score2);
    }

    public void ScoreText(int value){
        score.text = m_score1.ToString();
    }

	
	
	public void RecordText(int value){
	if (m_score1 > m_score2) {
			m_score2 = m_score1;
	}
	//else { m_score2 = m_score2 }
        score.text = m_score2.ToString();
    }



    public void PulsaPlayAgain(){
        SceneManager.LoadScene("Game");
    }

    public void PulsaExit(){
        SceneManager.LoadScene("MainMenu");
    }
}